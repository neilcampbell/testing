﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;
using Testing.Domain;
using Testing.Infrastructure.Persistence;
using Testing.Web.Controllers;
using Testing.Web.Infrastructure.Mapping;
using Testing.Web.Models.Registration;

namespace Testing.UnitTests.Controllers
{
    [TestClass]
    public class RegistrationControllerTests
    {
        private RegistrationViewModel _registrationViewModel;
        private IRegistrationTranslator _registrationTranslator;
        private IRepository<Registration> _registrationRepository;
        private RegistrationController _controller;

        [TestInitialize]
        public void Setup()
        {
            _registrationViewModel = new RegistrationViewModel
            {
                FirstName = "First",
                LastName = "Last",
                Email = "first@last.com"
            };
            _registrationTranslator = Substitute.For<IRegistrationTranslator>();
            _registrationRepository = Substitute.For<IRepository<Registration>>();

            _registrationTranslator.Translate(Arg.Any<RegistrationViewModel>()).ReturnsForAnyArgs(
                x => new Registration
                         {
                             FirstName = _registrationViewModel.FirstName,
                             LastName = _registrationViewModel.LastName,
                             Email = _registrationViewModel.Email
                         });
            
            _controller = new RegistrationController(_registrationTranslator, _registrationRepository);
        }

        [TestMethod]
        public void Index_WhenCalledWithViewModel_CallsTheRegistrationTranslator()
        {
            //Arrange

            //Act
            _controller.Index(_registrationViewModel);

            //Assert
            _registrationTranslator.Received(1).Translate(Arg.Is<RegistrationViewModel>(x => x == _registrationViewModel));
        }

        [TestMethod]
        public void Index_WhenCalledWithViewModel_CallsAddOnRegistrationRepository()
        {
            //Arrange

            //Act
            _controller.Index(_registrationViewModel); //Doesnt matter what I pass in here as the mocked call to translate will give me the correct data

            //Assert
            _registrationRepository.Received(1).Add(Arg.Is<Registration>(x =>
                x.FirstName == _registrationViewModel.FirstName &&
                x.LastName == _registrationViewModel.LastName &&
                x.Email == _registrationViewModel.Email));
        }
    }
}
