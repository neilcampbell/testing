﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Should.Fluent;
using Testing.Web.Infrastructure.Mapping;
using Testing.Web.Models.Registration;

namespace Testing.UnitTests.Infrastructure.Mapping
{
    [TestClass]
    public class RegistrationTranslatorTests
    {
        private IRegistrationTranslator _translator;

        [TestInitialize]
        public void Setup()
        {
            _translator = new RegistrationTranslator();
        }

        [TestMethod]
        public void Translate_WhenCalledWithValidRegistrationViewModel_ShouldMapToRegistration()
        {
            //Arrange
            var model = new RegistrationViewModel
                            {
                                FirstName = "First",
                                LastName = "Last",
                                Email = "first@last.com"
                            };

            //Act
            var registration = _translator.Translate(model);

            //Assert
            registration.FirstName.Should().Equal(model.FirstName);
            registration.LastName.Should().Equal(model.LastName);
            registration.Email.Should().Equal(model.Email);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void Translate_WhenCalledWithNullViewModel_ShouldThrow()
        {
            //Arrange

            //Act
            var registration = _translator.Translate(null);

            //Assert
        }
    }
}
