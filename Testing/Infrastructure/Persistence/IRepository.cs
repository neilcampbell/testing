﻿namespace Testing.Infrastructure.Persistence
{
    public interface IRepository<in T> where T : class
    {
        void Add(T entity);
    }
}
