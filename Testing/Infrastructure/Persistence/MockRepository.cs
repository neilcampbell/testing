﻿using System.Collections.Generic;

namespace Testing.Infrastructure.Persistence
{
    public class MockRepository<T> : IRepository<T> where T : class
    {
        private static readonly IList<T> InMemoryStore = new List<T>();

        public void Add(T entity)
        {
            InMemoryStore.Add(entity);
        }
    }
}
