﻿using Autofac;

namespace Testing.Infrastructure.Persistence
{
    public class PersistenceModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterGeneric(typeof(MockRepository<>))
                .As(typeof(IRepository<>));
        }
    }
}
