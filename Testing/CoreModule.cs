﻿using Autofac;
using Testing.Infrastructure.Persistence;

namespace Testing
{
    public class CoreModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterModule<PersistenceModule>();
        }
    }
}
