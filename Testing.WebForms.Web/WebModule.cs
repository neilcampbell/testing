﻿using Autofac;
using Testing.WebForms.Web.Infrastructure.Mapping;

namespace Testing.WebForms.Web
{
    public class WebModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterModule<MappingModule>();
        }
    }
}