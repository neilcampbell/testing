﻿using Autofac;

namespace Testing.WebForms.Web.Infrastructure.Mapping
{
    public class MappingModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterAssemblyTypes(typeof(WebFormsApplication).Assembly)
                .Where(t => t.Namespace.StartsWith("Testing.WebForms.Web.Infrastructure.Mapping"))
                .Where(t => t.Name.EndsWith("Translator"))
                .AsImplementedInterfaces();
        }
    }
}