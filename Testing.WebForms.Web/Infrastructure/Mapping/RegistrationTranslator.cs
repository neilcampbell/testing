﻿using System;
using Testing.Domain;
using Testing.WebForms.Web.Components.RegisterForm;

namespace Testing.WebForms.Web.Infrastructure.Mapping
{
    public class RegistrationTranslator : IRegistrationTranslator
    {
        public Registration Translate(RegisterFormViewModel model)
        {
            if (model == null)
                throw new ArgumentNullException("model");

            return new Registration
                       {
                           FirstName = model.FirstName,
                           LastName = model.LastName,
                           Email = model.Email
                       };
        }
    }
}