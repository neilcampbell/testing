﻿using Testing.Domain;
using Testing.WebForms.Web.Components.RegisterForm;

namespace Testing.WebForms.Web.Infrastructure.Mapping
{
    public interface IRegistrationTranslator
    {
        Registration Translate(RegisterFormViewModel model);
    }
}