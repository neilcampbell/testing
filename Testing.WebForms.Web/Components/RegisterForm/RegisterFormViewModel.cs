﻿namespace Testing.WebForms.Web.Components.RegisterForm
{
    public class RegisterFormViewModel
    {
        public string FormTitle { get; set; }

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
    }
}