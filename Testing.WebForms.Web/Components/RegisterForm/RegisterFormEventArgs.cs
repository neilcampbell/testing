﻿using System;

namespace Testing.WebForms.Web.Components.RegisterForm
{
    public class RegisterFormEventArgs : EventArgs
    {
        public RegisterFormViewModel Model { get; set; }
    }
}