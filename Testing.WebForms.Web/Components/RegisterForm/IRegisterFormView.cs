﻿using System;
using WebFormsMvp;

namespace Testing.WebForms.Web.Components.RegisterForm
{
    public interface IRegisterFormView : IView<RegisterFormViewModel>
    {
        event EventHandler<RegisterFormEventArgs> Submit;
        void RedirectToConfirmation();
    }
}
