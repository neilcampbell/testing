﻿using System;
using Testing.Domain;
using Testing.Infrastructure.Persistence;
using Testing.WebForms.Web.Infrastructure.Mapping;
using WebFormsMvp;

namespace Testing.WebForms.Web.Components.RegisterForm
{
    public class RegisterFormPresenter : Presenter<IRegisterFormView>
    {
        private readonly IRegistrationTranslator _registrationTranslator;
        private readonly IRepository<Registration> _registrationRepository;

        public RegisterFormPresenter(
            IRegisterFormView view,
            IRegistrationTranslator registrationTranslator,
            IRepository<Registration> registrationRepository) 
            : base(view)
        {
            _registrationTranslator = registrationTranslator;
            _registrationRepository = registrationRepository;

            View.Load += View_Load;
            View.Submit += View_Submit;
        }

        public void View_Load(object sender, EventArgs e)
        {
            //Demonstrates model binding back to the view
            View.Model.FormTitle = "Please register below";
        }

        public void View_Submit(object sender, RegisterFormEventArgs e)
        {
            var model = e.Model;
            var registration = _registrationTranslator.Translate(model);
            _registrationRepository.Add(registration);

            View.RedirectToConfirmation();
        }
    }
}