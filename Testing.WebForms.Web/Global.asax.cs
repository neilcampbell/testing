﻿using System;
using Autofac;
using Autofac.Integration.Web;
using WebFormsMvp.Autofac;
using WebFormsMvp.Binder;

namespace Testing.WebForms.Web
{
    public class WebFormsApplication : System.Web.HttpApplication, IContainerProviderAccessor
    {
        private static IContainerProvider _containerProvider;

        public IContainerProvider ContainerProvider
        {
            get { return _containerProvider; }
        }

        protected void Application_Start(object sender, EventArgs e)
        {
            ConfigureAutofac();
        }

        private void ConfigureAutofac()
        {
            var builder = new ContainerBuilder();

            builder.RegisterModule<CoreModule>();
            builder.RegisterModule<WebModule>();

            var container = builder.Build();
            _containerProvider = new ContainerProvider(container);

            PresenterBinder.Factory = new AutofacPresenterFactory(container);
        }
    }
}