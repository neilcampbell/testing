﻿using System;
using Testing.WebForms.Web.Infrastructure.MVC.Sitemap;

namespace Testing.WebForms.Web.Views.Home
{
    public partial class Index : System.Web.UI.Page
    {
        protected void Page_Init(object sender, EventArgs e)
        {
            PageTitles.CurrentPageTitle = PageTitles.Home.Index;
        }
    }
}