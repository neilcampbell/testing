﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Layout.master" AutoEventWireup="true" CodeBehind="Confirmation.aspx.cs" Inherits="Testing.WebForms.Web.Views.Register.Confirmation" %>

<asp:Content ContentPlaceHolderID="BodyContent" runat="server">
    <div class="row">
        <div class="span12">
            <h2>Thanks for registering</h2>
            <p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui. </p>
        </div>
    </div>
</asp:Content>