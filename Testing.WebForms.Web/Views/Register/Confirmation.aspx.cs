﻿using System;
using Testing.WebForms.Web.Infrastructure.MVC.Sitemap;

namespace Testing.WebForms.Web.Views.Register
{
    public partial class Confirmation : System.Web.UI.Page
    {
        protected void Page_Init(object sender, EventArgs e)
        {
            PageTitles.CurrentPageTitle = PageTitles.Registration.Confirmation;
        }
    }
}