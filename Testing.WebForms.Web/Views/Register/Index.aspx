﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Layout.master" AutoEventWireup="true" CodeBehind="Index.aspx.cs" Inherits="Testing.WebForms.Web.Views.Register.Index" %>
<%@ Register Src="~/Views/UserControls/RegisterForm.ascx" TagPrefix="testingControl" TagName="RegisterForm" %>

<asp:Content ContentPlaceHolderID="BodyContent" runat="server">
    <div class="row">
        <div class="span12">
            <h2>Register</h2>
            <testingControl:RegisterForm runat="server" id="RegisterForm" />
        </div>
    </div>
</asp:Content>