﻿using System;
using Testing.WebForms.Web.Components.RegisterForm;
using WebFormsMvp;
using WebFormsMvp.Web;

namespace Testing.WebForms.Web.Views.UserControls
{
    [PresenterBinding(typeof(RegisterFormPresenter))]
    public partial class RegisterForm : MvpUserControl<RegisterFormViewModel>, IRegisterFormView
    {
        public bool IsPostback
        {
            get { return IsPostBack; }
        }

        protected void Page_Init(object sender, EventArgs e)
        {

        }

        public void ClearField()
        {
            TxtFirstName.Text = String.Empty;
            TxtLastName.Text = String.Empty;
            TxtEmail.Text = String.Empty;
        }

        public event EventHandler<RegisterFormEventArgs> Submit;
        public void RedirectToConfirmation()
        {
            Response.Redirect("~/Views/Register/Confirmation.aspx", false);
        }

        protected void BtnSubmitRegistration_Click(object sender, EventArgs e)
        {
            if (!Page.IsValid) { return; }

            OnSubmit();
        }

        private void OnSubmit()
        {
            if (Submit != null)
                Submit(this, new RegisterFormEventArgs() { Model = ModelBind() });
        }

        private RegisterFormViewModel ModelBind()
        {
            return new RegisterFormViewModel
            {
                FirstName = TxtFirstName.Text,
                LastName = TxtLastName.Text,
                Email = TxtEmail.Text
            };
        }
    }
}