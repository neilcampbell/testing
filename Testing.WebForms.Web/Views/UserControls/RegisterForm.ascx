﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="RegisterForm.ascx.cs" Inherits="Testing.WebForms.Web.Views.UserControls.RegisterForm" %>

<form runat="server">
    <fieldset>
        <legend><%= Model.FormTitle %></legend>
        <ol>
            <li class="control-group">
                <label for="<%= TxtFirstName.ClientID %>" class="control-label">FirstName</label>
                <div class="controls">
                    <asp:TextBox ID="TxtFirstName" runat="server" />
                    <asp:RequiredFieldValidator ID="RfvFirstName" runat="server" ControlToValidate="TxtFirstName" Text="FirstName is required" />
                </div>
            </li>
            <li class="control-group">
                <label for="<%= TxtLastName.ClientID %>" class="control-label">LastName</label>
                <div class="controls">
                    <asp:TextBox ID="TxtLastName" runat="server" />
                    <asp:RequiredFieldValidator ID="RfvLastName" runat="server" ControlToValidate="TxtLastName" Text="LastName is required" />
                </div>
            </li>
            <li class="control-group">
                <label for="<%= TxtEmail.ClientID %>" class="control-label">Email</label>
                <div class="controls">
                    <asp:TextBox ID="TxtEmail" runat="server" />
                    <asp:RequiredFieldValidator ID="RfvEmail" runat="server" ControlToValidate="TxtEmail" Text="Email is required" />
                    <asp:RegularExpressionValidator ID="RevEmail" runat="server" ControlToValidate="TxtEmail" Text="Email is invalid" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" />
                </div>
            </li>
            <li class="form-actions">
                <asp:Button ID="BtnSubmitRegistration" runat="server" Text="Register" onclick="BtnSubmitRegistration_Click" CssClass="btn-primary" />
            </li>
        </ol>
    </fieldset>
</form>