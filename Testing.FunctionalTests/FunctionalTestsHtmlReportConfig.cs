﻿using TestStack.BDDfy.Processors.HtmlReporter;

namespace Testing.FunctionalTests
{
    public class FunctionalTestsHtmlReportConfig : DefaultHtmlReportConfiguration
    {
        public override string OutputFileName
        {
            get
            {
                return "Testing.FunctionalTests.html";
            }
        }

        public override string ReportHeader
        {
            get
            {
                return "Testing Functional Test Report";
            }
        }

        public override string ReportDescription
        {
            get
            {
                return "Functional Tests";
            }
        }
    }
}
