﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Should.Fluent;
using TestStack.BDDfy;
using TestStack.BDDfy.Core;
using TestStack.BDDfy.Scanners.StepScanners.Fluent;
using Testing.FunctionalTests.PageObjects;
using Testing.FunctionalTests.PageObjects.Registration;
using Testing.Web.Infrastructure.MVC.Sitemap;
using Testing.Web.Models.Registration;

namespace Testing.FunctionalTests.Tests.Registration
{
    [TestClass]
    [Story(
        AsA = "As an unregistered website user",
        IWant = "I want to register on the website",
        SoThat = "So that I recieve notifications of latest news and deals")]
    public class UserShouldBeAbleToRegister
    {
        private RegistrationPage _registrationPage;
        private RegistrationConfirmation _confirmationPage;

        [TestMethod]
        public void VerifyUserCanRegister()
        {
            this.Given(_ => GivenIAmAnUnregisteredWebsiteUser())
                .And(_ => AndINavigateToTheRegistrationPage())
                .When(_ => WhenICompleteTheRegistrationForm())
                .Then(_ => ThenIRecieveARegistrationConfirmation())
                .BDDfy();
        }

        private void GivenIAmAnUnregisteredWebsiteUser()
        {
            //Nothing required here
        }

        private void AndINavigateToTheRegistrationPage()
        {
            _registrationPage = new HomePage().Menu.NavigateToRegisterPage();
        }

        private void WhenICompleteTheRegistrationForm()
        {
            var registrationModel = new RegistrationViewModel
                                        {
                                            FirstName = "John",
                                            LastName = "Smith",
                                            Email = "john@smith.com.au"
                                        };
            _confirmationPage = _registrationPage.RegisterUser(registrationModel);
        }

        private void ThenIRecieveARegistrationConfirmation()
        {
            _confirmationPage.Title.Should().Equal(PageTitles.Prefix + PageTitles.Registration.Confirmation);
        }
    }
}
