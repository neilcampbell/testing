﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using TestStack.BDDfy;
using TestStack.BDDfy.Core;
using TestStack.BDDfy.Scanners.StepScanners.Fluent;
using Testing.FunctionalTests.PageObjects;
using Testing.FunctionalTests.PageObjects.Registration;
using Testing.Web.Models.Registration;

namespace Testing.FunctionalTests.Tests.Registration
{
    [TestClass]
    [Story(
        AsA = "As a website owner",
        IWant = "I dont want users who enter invalid data to be able register",
        SoThat = "So that we have an accurate picture of our user")]
    public class UsersWhoEnterInvalidDataShouldNotBeAbleToRegister
    {
        private RegistrationPage _registrationPage;

        [TestMethod]
        public void VerifyUserWhoEntersOnlySomeRequiredDataCannotSuccessfullyRegister()
        {
            this.Given(_ => GivenIAmAnUnregisteredWebsiteUser())
                .And(_ => AndINavigateToTheRegistrationPage())
                .When(_ => WhenICompleteTheRegistrationFormWithOnlySomeRequiredFields())
                .Then(_ => ThenIRecieveAValidationError())
                .BDDfy();
        }

        [TestMethod]
        public void VerifyUserWhoEnterInvalidEmailCannotSuccessfullyRegister()
        {
            this.Given(_ => GivenIAmAnUnregisteredWebsiteUser())
                .And(_ => AndINavigateToTheRegistrationPage())
                .When(_ => WhenICompleteTheRegistrationFormWithAnInvalidEmail())
                .Then(_ => ThenIRecieveAValidationError())
                .BDDfy();
        }

        private void GivenIAmAnUnregisteredWebsiteUser()
        {
            //Nothing required here
        }

        private void AndINavigateToTheRegistrationPage()
        {
            _registrationPage = new HomePage().Menu.NavigateToRegisterPage();
        }

        private void WhenICompleteTheRegistrationFormWithOnlySomeRequiredFields()
        {
            var registrationModel = new RegistrationViewModel
            {
                FirstName = "John",
                Email = "john@smith.com.au"
            };
            _registrationPage = _registrationPage.RegisterUserWithInvalidData(registrationModel);
        }

        private void WhenICompleteTheRegistrationFormWithAnInvalidEmail()
        {
            var registrationModel = new RegistrationViewModel
            {
                FirstName = "John",
                LastName = "Smith",
                Email = "invalidemail"
            };
            _registrationPage = _registrationPage.RegisterUserWithInvalidData(registrationModel);
        }

        private void ThenIRecieveAValidationError()
        {
            _registrationPage.HasValidationErrors();
        }
    }
}
