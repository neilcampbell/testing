﻿using OpenQA.Selenium;
using TestStack.Seleno.PageObjects;
using Testing.FunctionalTests.PageObjects.Components;
using Testing.Web.Models.Registration;

namespace Testing.FunctionalTests.PageObjects.Registration
{
    public class RegistrationPage : Page<RegistrationViewModel>
    {
        //We could also do it like this
        /*public string FirstName { set { Execute().ActionOnLocator(By.Name("FirstName"), e => e.ClearAndSendKeys(value)); } }
        public string LastName { set { Execute().ActionOnLocator(By.Name("LastName"), e => e.ClearAndSendKeys(value)); } }
        public string Email { set { Execute().ActionOnLocator(By.Name("Email"), e => e.ClearAndSendKeys(value)); } }
         
        public HomePage SubmitRegistration()
        {
            return Navigate().To<HomePage>(By.CssSelector("input[type='submit']"));
        }*/

        public GlobalNavigation Menu { get { return GetComponent<GlobalNavigation>(); } }

        public RegistrationConfirmation RegisterUser(RegistrationViewModel model)
        {
            Input().Model(model);
            return Navigate().To<RegistrationConfirmation>(By.CssSelector("input[type=\"submit\"]"));
        }

        public RegistrationPage RegisterUserWithInvalidData(RegistrationViewModel model)
        {
            Input().Model(model);
            return Navigate().To<RegistrationPage>(By.CssSelector("input[type=\"submit\"]"));
        }

        public void HasValidationErrors()
        {
            AssertThatElements(By.ClassName("field-validation-error")).Exist();
        }
    }
}
