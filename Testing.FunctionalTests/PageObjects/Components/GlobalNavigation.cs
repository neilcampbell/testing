using OpenQA.Selenium;
using TestStack.Seleno.PageObjects;
using Testing.FunctionalTests.PageObjects.Registration;

namespace Testing.FunctionalTests.PageObjects.Components
{
    public class GlobalNavigation : UiComponent
    {
        public RegistrationPage NavigateToRegisterPage()
        {
            return Navigate().To<RegistrationPage>(By.LinkText("Register"));
        }
    }
}