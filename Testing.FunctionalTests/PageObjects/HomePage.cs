﻿using TestStack.Seleno.PageObjects;
using Testing.FunctionalTests.PageObjects.Components;
using Testing.Web.Models.Home;

namespace Testing.FunctionalTests.PageObjects
{
    public class HomePage : Page<HomeViewModel>
    {
        public GlobalNavigation Menu { get { return GetComponent<GlobalNavigation>(); } }
    }
}
