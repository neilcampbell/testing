﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using TestStack.BDDfy.Configuration;
using TestStack.BDDfy.Processors.HtmlReporter;
using TestStack.Seleno.Configuration;
using TestStack.Seleno.Configuration.Screenshots;

namespace Testing.FunctionalTests
{
    [TestClass]
    public class AssemblyFixture
    {
        [AssemblyInitialize]
        public static void SetUp(TestContext testContext)
        {
            SelenoApplicationRunner.Run("Testing.Web", 12345, c => c.UsingCamera(new FileCamera("c:\\temp\\testing")));
            InitialiseBDDfyReport();
        }

        private static void InitialiseBDDfyReport()
        {
            Configurator.BatchProcessors.HtmlReport.Enable();
            Configurator.BatchProcessors.Add(new HtmlReporter(new FunctionalTestsHtmlReportConfig()));
        }
    }
}
