﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using TestStack.Seleno.Configuration;

namespace Testing.FunctionalTests
{
    public class WebTestBase
    {
        [AssemblyInitialize] //Try with nunit
        public void SetupTest()
        {
            SelenoApplicationRunner.Run("Testing.Web", 19457);
        }
    }
}
