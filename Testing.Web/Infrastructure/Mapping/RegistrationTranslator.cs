﻿using System;
using Testing.Domain;
using Testing.Web.Models.Registration;

namespace Testing.Web.Infrastructure.Mapping
{
    public class RegistrationTranslator : IRegistrationTranslator
    {
        public Registration Translate(RegistrationViewModel model)
        {
            if (model == null)
                throw new ArgumentNullException("model");

            return new Registration
                       {
                           FirstName = model.FirstName,
                           LastName = model.LastName,
                           Email = model.Email
                       };
        }
    }
}