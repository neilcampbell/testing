﻿using Autofac;

namespace Testing.Web.Infrastructure.Mapping
{
    public class MappingModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterAssemblyTypes(typeof(MvcApplication).Assembly)
                .Where(t => t.Namespace.StartsWith("Testing.Web.Infrastructure.Mapping"))
                .Where(t => t.Name.EndsWith("Translator"))
                .AsImplementedInterfaces();
        }
    }
}