﻿using Testing.Domain;
using Testing.Web.Models.Registration;

namespace Testing.Web.Infrastructure.Mapping
{
    public interface IRegistrationTranslator
    {
        Registration Translate(RegistrationViewModel model);
    }
}