﻿namespace Testing.Web.Infrastructure.MVC.Sitemap
{
    public class PageTitles
    {
        public const string Prefix = "Test - ";

        public class Home
        {
            public const string Index = "Home";
        }

        public class Registration
        {
            public const string Index = "Registration";
            public const string Confirmation = "Confirmation";
        }
    }
}