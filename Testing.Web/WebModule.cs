﻿using Autofac;
using Testing.Web.Infrastructure.Mapping;

namespace Testing.Web
{
    public class WebModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterModule<MappingModule>();
        }
    }
}