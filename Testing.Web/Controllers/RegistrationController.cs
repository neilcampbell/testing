﻿using System.Web.Mvc;
using Testing.Domain;
using Testing.Infrastructure.Persistence;
using Testing.Web.Infrastructure.Mapping;
using Testing.Web.Models.Registration;

namespace Testing.Web.Controllers
{
    public class RegistrationController : Controller
    {
        private readonly IRegistrationTranslator _registrationTranslator;
        private readonly IRepository<Registration> _registrationRepository;
        
        public RegistrationController(
            IRegistrationTranslator registrationTranslator,
            IRepository<Registration> registrationRepository)
        {
            _registrationTranslator = registrationTranslator;
            _registrationRepository = registrationRepository;
        }

        [HttpGet]
        public ActionResult Index()
        {
            var model = new RegistrationViewModel();
            return View(model);
        }

        [HttpPost]
        public ActionResult Index(RegistrationViewModel model)
        {
            var registration = _registrationTranslator.Translate(model);
            _registrationRepository.Add(registration);

            return RedirectToAction("Confirmation");
        }

        public ActionResult Confirmation()
        {
            return View();
        }
    }
}
