﻿using System.ComponentModel.DataAnnotations;

namespace Testing.Web.Models.Registration
{
    public class RegistrationViewModel
    {
        [Required]
        public string FirstName { get; set; }

        [Required]
        public string LastName { get; set; }

        [Required]
        [EmailAddress]
        public string Email { get; set; }
    }
}